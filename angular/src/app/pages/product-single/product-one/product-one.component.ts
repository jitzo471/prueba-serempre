import { Component, OnInit } from '@angular/core';
import { Lightbox } from 'ngx-lightbox';


@Component({
  selector: 'app-product-one',
  templateUrl: './product-one.component.html',
  styleUrls: ['./product-one.component.scss']
})
export class ProductOneComponent implements OnInit {

  public productAddedToCart;
  public productTotal;

  constructor(private _lightbox: Lightbox) {
    this.loadAlbum();
  }

  loadAlbum() {
    this.product.images.forEach(element => {
      const src = element.image;
      const caption = 'caption here';
      const thumb = element.image;
      const album = {
        src: src,
        caption: caption,
        thumb: thumb
      };
      this._album.push(album);
    });
  }

  private _album = [];

  open(index: number): void {

    // open lightbox
    console.log('image');
    this._lightbox.open(this._album, index);
  }

  close(): void {
    // close lightbox programmatically
    this._lightbox.close();
  }

  ngOnInit() {

  }

  public product = {
    products_id: 1,
    products_image: 'https://www.tendenciadigital.com.co/wp-content/uploads/2020/09/Audifonos-QuietConfort-Bose.jpg',
    products_price: 295.95,
    products_name: 'MOMENTUM True Wireless 2',
    products_subname: 'Earbuds that put sound first',
    products_brand: 'Earbuds',
    products_battery: '3-4 Hours',
    products_material: 'Plastic',
    products_style: 'Hans free',
    products_overview:
      'For the past 75 years, Sennheiser has put sound first. The new MOMENTUM True Wireless 2 is no different. Thanks to leading audio technology and innovation, these new earbuds deliver the best listening experience anytime, anywhere. With improved ergonomics designed for full day wearing and refined touch controls for a more personalised experience, they have been finely crafted for the most discerning listener and aim to simplify your life by enhancing your everyday.',
    images: [
      {
        "id": 1,
        "products_id": 1,
        "image": "assets/images/gallery/preview/Product_image_01.png",
        "htmlcontent": null,
        "sort_order": 1
      },
      {
        "id": 2,
        "products_id": 1,
        "image": "assets/images/gallery/preview/Product_image_02.png",
        "htmlcontent": null,
        "sort_order": 2
      },
      {
        "id": 3,
        "products_id": 1,
        "image": "assets/images/gallery/preview/Product_image_03.png",
        "htmlcontent": null,
        "sort_order": 3
      }
    ],
    specifications: [
      { key: 'Dimensions', value: '76.8 x 43.8 x 34.7 mm (earbuds and charging case)'},
      { key: 'USB Standar', value: 'USB-C' },
      { key: 'Power Supply', value: 'Sennheiser 7mm dynamiz driver' },
      { key: 'Frecuency response (Microphone)', value: '100 Hz to 10 kHz' },
      { key: 'Frecuency response', value: '5 - 21,000 Hz' },
      { key: 'Noise cancellation', value: 'Single-Mic ANC per earbud side' }
    ]
  };

  slideProductConfig = {
    "slidesToShow": 1,
    "slidesToScroll": 1,
    "infinite": false,
    //"asNavFor": "slideProductNavConfig",
    "asNavFor": ".thumbs",
  };

  slideProductNavConfig = {
    "slidesToShow": 3,
    "slidesToScroll": 1,
    "asNavFor": ".feedback",
    //"asNavFor": "slideProductConfig",
    "centerMode": true,
    "centerPadding": '60px',
    "dots": false,
    "arrows": false,
    "focusOnSelect": true
  };

  slideRelatedProductConfig = {
    "slidesToShow": 4,
    "slidesToScroll": 1,
    "dots": true,
    "infinite": false,

    responsive: [
      {
        breakpoint: 768,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]

  };
  slickInit(e) {
    console.log('slick initialized');
  }
}
