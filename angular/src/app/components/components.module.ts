import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { TranslateModule } from '@ngx-translate/core';
import { PipesModule } from 'src/app/pipes/pipes.module';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { LogoComponent } from './logo/logo.component';
import { PageLoaderComponent } from './page-loader/page-loader.component';
import { BackToTopComponent } from './back-to-top/back-to-top.component';
import { NgxSpinnerModule } from 'ngx-spinner';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

@NgModule({
  declarations: [LogoComponent, PageLoaderComponent, BackToTopComponent],
  imports: [
    CommonModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    TranslateModule,
    PipesModule,
    // Specify your library as an import
    SlickCarouselModule,
  ],
  exports: [LogoComponent, PageLoaderComponent, BackToTopComponent]
})
export class ComponentsModule { }
