import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { TranslateModule } from '@ngx-translate/core';

import { ComponentsModule } from 'src/app/components/components.module';
import { FooterMobileComponent } from './footer-mobile/footer-mobile.component';
import { FooterThreeComponent } from './footer-three/footer-three.component';





@NgModule({
  declarations: [FooterMobileComponent, FooterThreeComponent],
  imports: [

    CommonModule,
    AppRoutingModule,
    TranslateModule,
    ComponentsModule
  ],
  exports: [FooterMobileComponent, FooterThreeComponent]
})
export class FooterModule { }
