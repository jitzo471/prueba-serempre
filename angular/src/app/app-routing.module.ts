import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductOneComponent } from './pages/product-single/product-one/product-one.component';
import { NotfoundComponent } from './pages/notfound/notfound.component';

const routes: Routes = [
  { path: '', component: ProductOneComponent },
  { path: 'product-one', component: ProductOneComponent },
  { path: '**', component: NotfoundComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
